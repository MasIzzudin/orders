import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'

import { AppNavigation } from './src/router/appNavigation';
import configureStore from './src/store/configureStore';

const { store, persistor } = configureStore()



export default class App extends Component {
  render() {
    console.log(store.getState(), 'store')
    return (
      <Provider store={store} >
        <PersistGate loading={null} persistor={persistor} >
          <AppNavigation />
        </PersistGate>
      </Provider>
    );
  }
}
