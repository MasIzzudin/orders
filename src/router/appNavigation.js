import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

// Import Navigasi
//import {DrawerNavigation} from './drawerNavigation';
import Saldo from '../screens/saldo/saldo';
import AddSaldo from '../screens/saldo/addSaldo';
import DetailSaldo from '../screens/saldo/detailSaldo';



const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
);

const Root = createStackNavigator(
    {
      Saldo: Saldo,
      AddSaldo: AddSaldo,
      DetailSaldo: DetailSaldo
    },
    {
        headerMode: 'none'
    }
);

const AppWithNavigationState = reduxifyNavigator(Root, "root");

const mapStateToProps = state => ({
  state: state.nav,
});

const AppNavigation = connect(mapStateToProps)(AppWithNavigationState);

export { Root, AppNavigation, middleware };
