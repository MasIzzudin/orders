import React, { Component } from 'react';
import { Modal, View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import { Icon } from 'react-native-elements';

import { GlobalStyle } from '../lib/globalStyle';
import { GlobalFunction } from '../lib/globalFunction';
import { Button } from '../components/button';
import ModalContact from './modalContact';

class ModalTransfer extends Component {
	constructor(props) {
		super(props)
		this.state = {
			saldo: '',
			note: '',
			modalContact: false,
			itemSelected: null
		}
	}


	onCountingProccess() {
		if(this.state.itemSelected == null) {
			alert('silahkan isi kontak yang akan di tuju')
		} else if(this.state.saldo == '') {
			alert('isi saldo anda')
		} else {
			let dataTo = this.props.detail.list.filter(data => { return data.user_id == this.state.itemSelected.id })
			let saldoTo = dataTo.length === 0 ? 0 : dataTo[0].amount
			
			let itemTo = {
				id: this.props.detail.sum++,
				saldo: parseInt(saldoTo) + parseInt(this.state.saldo),
				amount: this.state.saldo,
				status: 'increment',
				note: `Dapat transferan dari ${this.props.title}`,
				date: GlobalFunction.getDateNow(),
				user_id: this.state.itemSelected.id
			}

			let itemFrom = {
				id: this.props.detail.sum++,
				saldo: parseInt(this.props.total) - parseInt(this.state.saldo),
				amount: this.state.saldo,
				status: 'decrement',
				note: `Transfer ke ${this.state.itemSelected.name}`,
				date: GlobalFunction.getDateNow(),
				user_id: this.props.user_id
			}

			let sendData = [ itemTo, itemFrom ]
			this.props.sendIncrement(sendData)
			this.props.closeModal()
		}
	}

	render() {
		return (
			<Modal
				animationType='fade'
				transparent
				visible={this.props.visible}
				onRequestClose={() => { }}
			>
				<View style={Style.container}>
					<View style={Style.content}>
						<View style={{ alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={this.props.closeModal}>
								<Icon name='ios-close-circle-outline' type='ionicon' />
							</TouchableOpacity>
						</View>
						<View style={Style.inputContent}>
							<TouchableOpacity style={{ borderWidth: 1, paddingLeft: 5 }} onPress={() => this.setState({ modalContact: true })}>
								{
									this.state.itemSelected == null ?
									<Text>Transfer Ke :</Text>
									:
									<Text>{this.state.itemSelected.name}</Text>
								}
							</TouchableOpacity>
							<TextField
								label='saldo'
								onChangeText={saldo => this.setState({ saldo })}
								keyboardType='numeric'
								value={this.state.saldo}
								style={{ textAlign: 'right', paddingRight: 5 }}
							/>
							{/* <TextField
								label='note'
								value={this.state.note}
								onChangeText={note => this.setState({ note })}
							/> */}
						</View>
						<View style={Style.buttonContainer}>
							<Button
								title='save'
								save
								onPress={() => this.onCountingProccess()}
							/>
						</View>
					</View>
				</View>
				<ModalContact 
					visible={this.state.modalContact}
					user_id={this.props.user_id}
					onCloseModal={() => this.setState({ modalContact: false })}
					selectItem={(item) => this.setState({ itemSelected: item })}
				/>
			</Modal>
		)
	}
}

const mapStateToProps = state => {
	return {
		saldo: state.saldo,
		detail: state.detail
	}
}

const mapDispatchToProps = dispatch => {
	return {
		sendIncrement: (data, saldo) => dispatch({ type: 'SALDO_INCREMENT', data })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalTransfer);

const Style = {
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(0,0,0,0.5)'
	},
	content: {
		backgroundColor: 'white',
		height: GlobalStyle.dimensionHeight / 2,
		width: GlobalStyle.dimensionWidth - 30,
		padding: 10
	},
	inputContent: {
		padding: 10,
		flex: 3,
		justifyContent: 'center',
	},
	buttonContainer: {
		flex: 1,
		alignItems: 'flex-end',
		flexDirection: 'column',
	}
}