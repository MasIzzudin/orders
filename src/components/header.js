import React from 'react';
import { Icon } from 'react-native-elements';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';

import { GlobalStyle } from '../lib/globalStyle';

const Width = Dimensions.get('window').width

export const Header = (props) => {
	return (
		<View style={Styles.header}>
			<View style={Styles.buttonContainer}>
				{
					props.back ?
						<TouchableOpacity onPress={props.onPress} style={{ alignItems: 'center' }}>
							<Icon name='arrow-back' color='white' />
						</TouchableOpacity>
						:
						<View />
				}

			</View>

			<View style={Styles.titleContainer}>
				<Text style={Styles.text}>{props.title.toUpperCase()}</Text>
			</View>

			<View style={Styles.wrapRight}>
				{
					props.delete &&
					<TouchableOpacity onPress={props.onDelete}>
						<Icon name='delete' type='ionicons' color='white' />
					</TouchableOpacity>
				}
			</View>
		</View>
	)
}

const Styles = {
	header: {
			flexDirection: 'row',
			backgroundColor: GlobalStyle.colorGreen,
			height: 50,
			width: Width,
			justifyContent: 'center',
			alignItems: 'center',
			paddingTop: 10,
			paddingBottom: 10,
	},
	text: {
			color: 'white'
	},
	buttonContainer: {
			flex: 1,
	},
	titleContainer: {
			flex: 4,
			justifyContent: 'center',
			alignItems: 'center',
	},
	wrapRight: {
			flex: 1,
	}
}
