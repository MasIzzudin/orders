import React, { Component } from 'react';
import { Modal, View, Text, ScrollView, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import { Icon } from 'react-native-elements';

import { GlobalStyle } from '../lib/globalStyle';
import { GlobalFunction } from '../lib/globalFunction';
import { Button } from '../components/button';

class ModalContact extends Component {
	constructor(props) {
		super(props)
		this.state = {
			saldo: '',
			note: '',
		}
	}

	_renderItem(item) {
		return (
			<TouchableOpacity style={Style.buttonListContent} onPress={() => this.onSelectItem(item)}>
				<View style={Style.listContainer}>
					<Text>{item.name}</Text>
				</View>
			</TouchableOpacity>
		)
	}

	onSelectItem(item) {
		this.props.selectItem(item)
		this.props.onCloseModal()
	}

	render() {
		let listSaldo = this.props.saldo.list.filter(data => { return data.id !== this.props.user_id })
		return (
			<Modal
				animationType='fade'
				transparent
				visible={this.props.visible}
				onRequestClose={() => { }}
			>
				<View style={Style.container}>
					<View style={Style.content}>
						<View style={{ alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={this.props.onCloseModal}>
								<Icon name='ios-close-circle-outline' type='ionicon' />
							</TouchableOpacity>
						</View>
						<View style={{ flex: 1 }}>
							{
								listSaldo.length !== 0 ?
									<FlatList
										data={listSaldo}
										extraData={this.state}
										keyExtractor={(item, index) => `list-item-${index}`}
										renderItem={({ item, index }) => this._renderItem(item)}
									/>
									:
									<View>
										<Text>no Data</Text>
									</View>
							}

						</View>
					</View>
				</View>
			</Modal>
		)
	}
}

const mapStateToProps = state => {
	return {
		saldo: state.saldo,
		detail: state.detail
	}
}

const mapDispatchToProps = dispatch => {
	return {
		sendIncrement: (data, saldo) => dispatch({ type: 'SALDO_INCREMENT', data })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalContact);

const Style = {
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(0,0,0,0.5)'
	},
	content: {
		backgroundColor: 'white',
		height: GlobalStyle.dimensionHeight / 1.5,
		width: GlobalStyle.dimensionWidth - 30,
		padding: 10
	},
	buttonListContent: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 20,
		borderWidth: 1,
	},
	listContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
		height: 50,
		width: 100,
	}
}