import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';

import { GlobalStyle } from '../lib/globalStyle';

export const ButtonRadius = (props) => {
	return (
		<View>
			{
				props.add ?
					<TouchableOpacity onPress={props.onPress} style={Style.buttonPlus}>
						<Icon name='ios-add' type='ionicon' color='white' />
					</TouchableOpacity>
					:
					<TouchableOpacity onPress={props.onPress} style={Style.buttonMinus}>
						<Icon name='ios-remove' type='ionicon' color='white' />
					</TouchableOpacity>
			}

		</View>
	)
}

const Style = {
	buttonPlus: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 60,
		height: 60,
		backgroundColor: GlobalStyle.greenOpacity,
		borderRadius: 50,
		elevation: 1,
		shadowColor: 'black',
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 5,
	},
	buttonMinus: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 60,
		height: 60,
		backgroundColor: GlobalStyle.redOpacity,
		borderRadius: 50,
		elevation: 1,
		shadowColor: 'black',
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 5,
	}
}