import React, { Component } from 'react';
import { Modal, View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import { Icon } from 'react-native-elements';

import { GlobalStyle } from '../lib/globalStyle';
import { GlobalFunction } from '../lib/globalFunction';
import { Button } from '../components/button';

class ModalAmount extends Component {
	constructor(props) {
		super(props)
		this.state = {
			saldo: '',
			note: ''
		}
	}


	onCountingProccess() {
		if (this.props.type == 'increment') {
			if (this.state.saldo !== '') {
				let saldo = parseInt(this.props.total) + parseInt(this.state.saldo)
				const data = {
					id: this.props.detail.sum++,
					saldo: saldo,
					amount: this.state.saldo,
					status: 'increment',
					note: this.state.note,
					date: GlobalFunction.getDateNow(),
					user_id: this.props.user_id
				}
				this.props.sendIncrement(data)
				this.setState({ saldo: '', note: '' })
				this.props.closeModal()
			} else {
				alert('saldo harus di isi')
			}
		} else if (this.props.type == 'decrement') {
			if (this.state.saldo !== '') {
				let saldo = parseInt(this.props.total) - parseInt(this.state.saldo)
				const data = {
					id: this.props.detail.sum++,
					saldo: saldo,
					amount: this.state.saldo,
					status: 'decrement',
					note: this.state.note,
					date: GlobalFunction.getDateNow(),
					user_id: this.props.user_id
				}
				this.props.sendIncrement(data)
				this.setState({ saldo: '', note: '' })
				this.props.closeModal()
			} else {
				alert('saldo harus di isi')
			}
		}
	}

	render() {
		return (
			<Modal
				animationType='fade'
				transparent
				visible={this.props.visible}
				onRequestClose={() => { }}
			>
				<View style={Style.container}>
					<View style={Style.content}>
						<View style={{ alignItems: 'flex-end' }}>
							<TouchableOpacity onPress={this.props.closeModal}>
								<Icon name='ios-close-circle-outline' type='ionicon' />
							</TouchableOpacity>
						</View>
						<View style={Style.inputContent}>
							<TextField
								label='saldo'
								onChangeText={saldo => this.setState({ saldo })}
								keyboardType='numeric'
								value={this.state.saldo}
								style={{ textAlign: 'right', paddingRight: 5 }}
							/>
							<TextField
								label='note'
								value={this.state.note}
								onChangeText={note => this.setState({ note })}
							/>
						</View>
						<View style={Style.buttonContainer}>
							<Button
								title='save'
								save
								onPress={() => this.onCountingProccess()}
							/>
						</View>
					</View>
				</View>
			</Modal>
		)
	}
}

const mapStateToProps = state => {
	return {
		detail: state.detail
	}
}

const mapDispatchToProps = dispatch => {
	return {
		sendIncrement: (data, saldo) => dispatch({ type: 'SALDO_INCREMENT', data })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalAmount);

const Style = {
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(0,0,0,0.5)'
	},
	content: {
		backgroundColor: 'white',
		height: GlobalStyle.dimensionHeight / 2,
		width: GlobalStyle.dimensionWidth - 30,
		padding: 10
	},
	inputContent: {
		padding: 10,
		flex: 3,
		justifyContent: 'center',
	},
	buttonContainer: {
		flex: 1,
		alignItems: 'flex-end',
		flexDirection: 'column',
	}
}