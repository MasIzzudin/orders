import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import { GlobalStyle } from '../lib/globalStyle';

export const Button = (props) => {
	return (
		<View>
			<TouchableOpacity onPress={props.onPress} 
			style={[Style.button, props.save ? { backgroundColor: GlobalStyle.colorGreen } : props.cancel ? { backgroundColor: GlobalStyle.colorRed } : {backgroundColor: 'transparent'} ]}>
				<Text style={{ color: 'white' }}>{props.title}</Text>
			</TouchableOpacity>
		</View>
	)
}

const Style = {
	button: {
		width: 80,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
		elevation: 3,
		shadowColor: 'black',
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 5,
		margin: 5,
		marginRight: GlobalStyle.dimensionWidth / 35,
		marginBottom: GlobalStyle.dimensionHeight / 5
	}
}