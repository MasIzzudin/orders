const INITIAL_STATE = {
	list: [],
	total: 0,
	sum: 0
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'SALDO_INCREMENT':
		return {
			...state,
			list: [ ...state.list, action.data]
		}
		case 'DELETE_ITEM' :
		return {
			...state,
			list: state.list.filter(data => data.user_id !== action.id)
		}
		case 'DELETE_LIST_SALDO' :
			return {
				...state,
				list: state.list.filter(data => data.user_id !== action.id)
			}
		case 'DELETE_SINGLE_ITEM' :
			return {
				...state,
				list: state.list.filter(data => data.id !== action.item.id)
			}
		default:
			return state
	}
}