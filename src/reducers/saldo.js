const INITIAL_STATE = {
	sum: 0,
	list: [],
	totalAmount: 0
}

export default (state = INITIAL_STATE, action) => {
	console.log(action)
	switch (action.type) {
		case 'ADD_LIST_SALDO':
			return {
				...state,
				list: state.list.concat(action.payload)
			}
		case 'EDIT_LIST_SALDO':
		const list = state.list.map(data => {
			if (data.id == action.id) {
				return { ...data, ...action.payload }
			}
			return data
		})
		return {
			...state,
			list: list
		}
		case 'DELETE_LIST_SALDO':
			return {
				...state,
				list: state.list.filter(data => data.id !== action.id)
			}
		case 'SALDO_INCREMENT':
			if (action.data.status == 'increment') {
				return {
					...state,
					totalAmount: parseInt(action.data.amount) + parseInt(state.totalAmount)
				}
			} else if (action.data.status == 'decrement') {
				return {
					...state,
					totalAmount: parseInt(state.totalAmount) - parseInt(action.data.amount)
				}
			}
		case 'DELETE_SINGLE_ITEM':
			if (action.item.status == 'increment') {
				return {
					...state,
					totalAmount: parseInt(state.totalAmount) - parseInt(action.item.amount)
				}
			} else if (action.item.status == 'decrement') {
				return {
					...state,
					totalAmount: parseInt(state.totalAmount) + parseInt(action.item.amount)
				}
			}
		case 'DELETE_TOTAL_SALDO':
			if (action.params.status == 'increment') {
				return {
					...state,
					totalAmount: parseInt(state.totalAmount) - parseInt(action.params.saldo)
				}
			} else if (action.params.status == 'decrement') {
				return {
					...state,
					totalAmount: parseInt(state.totalAmount) - parseInt(action.params.saldo)
				}
			}
		case 'RESET_INDEX':
			return {
				...state,
				sum: 0
			}
		default:
			return state;
	}
}