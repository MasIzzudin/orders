import { combineReducers } from 'redux';

import nav from './nav';
import saldo from './saldo';
import detail from './detail'

export default combineReducers({
		nav,
		saldo,
		detail
})