import { NavigationActions } from 'react-navigation';

import { Root } from '../router/appNavigation';

const firstAction = Root.router.getActionForPathAndParams('Saldo')
const initialState = Root.router.getStateForAction(firstAction)

export default (state = initialState, action) => {
    let nextState;
    switch (action.type) {
        default:
            nextState = Root.router.getStateForAction(action, state)
    }


    return nextState || state;
}