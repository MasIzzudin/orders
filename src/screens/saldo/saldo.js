import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';
import { ListItem, List, Icon } from 'react-native-elements';

import { ButtonRadius } from '../../components/buttonRadius';
import { GlobalFunction } from '../../lib/globalFunction';
import { GlobalStyle } from '../../lib/globalStyle';
import { Header } from '../../components/header';


class Saldo extends Component {
	constructor(props) {
		super(props)
		this.state = {
			itemMenu: null
		}
	}

	addSaldoRequest() {
		this.props.navigation.navigate('AddSaldo', { status: 'add' })
	}

	redirectDetailData(item) {
		this.props.navigation.navigate('DetailSaldo', { item })
	}

	redirectEditData(item) {
		this.props.navigation.navigate('AddSaldo', { status: 'edit', item })
	}

	openMenu(item) {
		if (this.state.itemMenu == item.id) {
			this.setState({ itemMenu: null })
		} else {
			this.setState({ itemMenu: item.id })
		}
	}

	deleteData(item) {
		let listSaldo;
		if (this.props.detail.list.length !== 0) {
			listSaldo = this.props.detail.list.filter(data => {
				return data.user_id == item.id
			})
		} else {
			listSaldo = []
		}
		Alert.alert(
			'Perhatian!!',
			'Anda yakin ingin menghapus user ini? semua saldo milik user ini akan ikut terhapus',
			[
				{ text: 'Yes', onPress: () => this.requestDeleteData(item.id, listSaldo) },
				{ text: 'No' }
			]
		)
	}

	requestDeleteData(item, list) {
		if (this.props.saldo.list.length == 1) {
			this.props.resetIndex()
		} 
		if (list.length !== 0) {
			let index = list.length - 1
			const saldo = list[index].saldo
			const status = list[index].status
			const data = {
				saldo: saldo,
				status: status
			}
			this.props.deleteSaldoAmount(data)
			this.props.deleteData(item)
		} else {
			this.props.deleteData(item)
		}
	}

	_renderItem(item) {
		return (
			<TouchableOpacity onPress={() => this.openMenu(item)}>
				<View style={Style.content}>
					<View style={{ height: 50, justifyContent: 'center', padding: 15, alignItems: 'center' }}>
						<Text>{item.name}</Text>
					</View>
					{
						item.id == this.state.itemMenu &&
						<View>
							<View style={Style.lineMenu} />
							<View style={Style.itemMenu}>
								<TouchableOpacity style={Style.editContainer} onPress={() => this.redirectEditData(item)}>
									<Icon name='create' type='ionicons' />
									<Text>Edit</Text>
								</TouchableOpacity>
								<View style={Style.lineHorizontal} />
								<TouchableOpacity style={Style.editContainer} onPress={() => this.deleteData(item)}>
									<Icon name='delete' type='ionicons' color={GlobalStyle.colorRed} />
									<Text>Delete</Text>
								</TouchableOpacity>
								<View style={Style.lineHorizontal} />
								<TouchableOpacity style={Style.editContainer} onPress={() => this.redirectDetailData(item)}>
									<Icon name='list' type='ionicons' color={GlobalStyle.colorGreen} />
									<Text>Saldo</Text>
								</TouchableOpacity>
							</View>
						</View>
					}

				</View>

				<View style={Style.line} />
			</TouchableOpacity>
		)
	}

	render() {
		return (
			<View style={Style.container}>
				<Header
					title='User'
				/>
				<View style={{ flex: 1 }}>
					{
						this.props.saldo.list.length !== 0 ?
							<List>
								<FlatList
									data={this.props.saldo.list}
									extraData={this.state}
									keyExtractor={(item, index) => `list-item-${index}`}
									renderItem={({ item, index }) => this._renderItem(item)}
								/>
							</List>
							:
							<View style={Style.noDataStyle}>
								<Text>No Data</Text>
							</View>
					}
				</View>
				<View style={Style.buttonStyleContainer}>
					<ButtonRadius
						onPress={() => this.addSaldoRequest()}
						add
					/>
				</View>
				<View style={Style.footerContent}>
					<View style={{ flex: 1, padding: 10 }}>
						<Text style={{ color: 'white' }}>Total Keseluruhan: </Text>
					</View>

					<View style={{ flex: 1.6, alignItems: 'flex-end', padding: 10 }}>
						<Text style={{ color: 'white' }}>{GlobalFunction.addCommas(this.props.saldo.totalAmount)}</Text>
					</View>
				</View>

			</View>
		)
	}
}

const mapDispatchToProps = dispatch => {
	return {
		detailSaldo: (data) => dispatch({ type: 'DETAIL', payload: data }),
		deleteData: (id) => dispatch({ type: 'DELETE_LIST_SALDO', id }),
		deleteSaldoAmount: (params) => dispatch({ type: 'DELETE_TOTAL_SALDO', params }),
		resetIndex: () => dispatch({ type: 'RESET_INDEX' })
	}
}

const mapStateToProps = state => {
	return {
		saldo: state.saldo,
		detail: state.detail
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Saldo);

const Style = {
	container: {
		flex: 1,
	},
	noDataStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonStyleContainer: {
		position: 'absolute',
		bottom: 70,
		right: 40,
		zIndex: 1,
	},
	buttonStyle: {
		backgroundColor: GlobalStyle.colorGreen,
		borderRadius: 50,
		width: 50, height: 50,
		elevation: 1,
		shadowColor: 'black',
		shadowOffset: { width: 10, height: 10 },
		shadowOpacity: 5,
	},
	content: {
		flex: 1
	},
	line: {
		backgroundColor: 'rgba(0,0,0,0.2)',
		height: 1
	},
	itemMenu: {
		backgroundColor: 'white',
		height: 60,
		flexDirection: 'row',
	},
	lineMenu: {
		backgroundColor: 'rgba(0,0,0,0.2)',
		height: 3
	},
	editContainer: {
		flex: 1,
		padding: 10,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	lineHorizontal: {
		backgroundColor: 'rgba(0,0,0,0.2)',
		width: 1
	},
	footerContent: {
		backgroundColor: GlobalStyle.colorGreen,
		elevation: 10,
		flexDirection: 'row',
		height: 50,
		alignItems: 'center',
	}
}