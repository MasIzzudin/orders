import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Dimensions, ScrollView } from 'react-native';
import { TextField } from 'react-native-material-textfield';

import { Header } from '../../components/header';
import { Button } from '../../components/button';
import { GlobalStyle } from '../../lib/globalStyle';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

class AddSaldo extends Component {
	constructor(props) {
		super(props)
		this.state = {
			name: '',
			note: ''
		}
	}

	componentDidMount() {
		console.log(this.props.navigation.state.params.item, 'lisst')
		if (this.props.navigation.state.params.status == 'edit') {
			this.setState({
				name: this.props.navigation.state.params.item.name,
				note: this.props.navigation.state.params.item.note
			})
		}
	}

	onBack() {
		this.props.navigation.goBack()
	}

	onSaveData() {
		console.log(this.props.navigation.state.params.status)
		if (this.props.navigation.state.params.status == 'edit') {
			let id = this.props.navigation.state.params.item.id;
			const dataEdit = {
				id: id,
				name: this.state.name,
				note: this.state.note
			}
			console.log(dataEdit, 'dataa')
			this.props.editData(dataEdit, id)
			this.props.navigation.navigate('Saldo')
		} else {
			let id = this.props.saldo.sum++;
			const data = {
				id: id,
				name: this.state.name,
				note: this.state.note
			}
			console.log(data, 'addData')
			this.props.addSaldo(data)
			this.props.navigation.navigate('Saldo')
		}

	}

	render() {
		return (
			<View style={Style.container}>
				<Header
					title='Add User'
					back
					onPress={() => this.onBack()}
				/>
				<ScrollView>
					<View style={Style.textInputContent}>
						<TextField
							label='nama'
							tintColor={GlobalStyle.colorGreen}
							value={this.state.name}
							onChangeText={name => this.setState({ name })}
						/>
						<TextField
							label='note'
							tintColor={GlobalStyle.colorGreen}
							value={this.state.note}
							onChangeText={note => this.setState({ note })}
						/>
					</View>
				</ScrollView>
				<View style={Style.buttonContainer}>
					<Button
						title='Cancel'
						cancel
						onPress={() => this.onBack()}
					/>
					{
						this.state.name !== '' &&
						<Button
							title='Save'
							save
							onPress={() => this.onSaveData()}
						/>
					}
				</View>
			</View>
		)
	}
}

const mapStateToProps = state => {
	return {
		saldo: state.saldo
	}
}

const mapDispatchToProps = dispatch => {
	return {
		addSaldo: (data) => dispatch({ type: 'ADD_LIST_SALDO', payload: data }),
		editData: (dataEdit, id) => dispatch({ type: 'EDIT_LIST_SALDO', payload: dataEdit, id})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSaldo);

const Style = {
	container: {
		flex: 1
	},
	textInputContent: {
		padding: 20,
	},
	buttonContainer: {
		flex: 1,
		justifyContent: 'flex-end',
		flexDirection: 'row',
	}
}