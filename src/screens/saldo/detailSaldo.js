import React, { Component } from 'react';
import { View, Text, FlatList, Alert, TouchableOpacity, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { ListItem, Icon } from 'react-native-elements';


import { Header } from '../../components/header';
import { ButtonRadius } from '../../components/buttonRadius';
import { GlobalStyle } from '../../lib/globalStyle';
import ModalAmount from '../../components/modalAmount';
import ModalTransfer from '../../components/modalTransfer';
import { GlobalFunction } from '../../lib/globalFunction';

class DetailSaldo extends Component {
	constructor(props) {
		super(props)
		this.state = {
			modalPlus: false,
			modalMinus: false,
			modalTransfer: false,
			total: 0,
			itemMenu: null,
			scrollPosition: 0,
      scrollHeight: 0,
      maxScroll: 0,
      maxHeight: 0
		}
	}

	onBack() {
		this.props.navigation.navigate("Saldo")
	}

	_onPlusButton(listSaldo) {
		if (listSaldo.length !== 0) {
			let index = listSaldo.length - 1
			let total = listSaldo[index].saldo
			this.setState({ total })
		}
		this.setState({ modalPlus: true })
	}

	_onTransferButton(listSaldo) {
		if (listSaldo.length !== 0) {
			let index = listSaldo.length - 1
			let total = listSaldo[index].saldo
			this.setState({ total })
		}
		this.setState({ modalTransfer: true })
	}

	_onMinusButton(listSaldo) {
		if (listSaldo.length !== 0) {
			let index = listSaldo.length - 1
			let total = listSaldo[index].saldo
			this.setState({ total })
			this.setState({ modalMinus: true })
		} else {
			alert('Saldo anda gak ada, mohon cari pinjaman/ngutang dulu')
		}
	}

	deleteItem(saldo) {
		if (saldo.length !== 0) {
			let user_id = this.props.navigation.state.params.item.id
			Alert.alert(
				'Perhatian!!',
				'Anda yakin ingin menghapus semua data?',
				[
					{ text: 'Yes', onPress: () => this._onDeleteItem(user_id, saldo) },
					{ text: 'No', }
				]
			)
		} else {
			alert('saldonya aja gak ada, apanya yang mau di hapus?')
		}

	}

	_onDeleteItem(user_id, saldo) {
		let index = saldo.length - 1
		const lastSaldo = saldo[index].saldo
		const status = saldo[index].status
		const data = {
			saldo: lastSaldo,
			status: status
		}
		this.props.deleteSaldoAmount(data)
		this.props.deleteItem(user_id)
		this.setState({ total: 0 })
	}

	_openMenu(item, listSaldo, index) {
		let id = listSaldo.length - 1
		if (index == id) {
			if (this.state.itemMenu == id) {
				this.setState({ itemMenu: null })
			} else {
				this.setState({ itemMenu: id })
			}
		}
	}

	_onDeleteSingleItem(item) {
		Alert.alert(
			'Perhatian!',
			'Apakah anda yakin?',
			[
				{ text: 'Yes', onPress: () => this.props._onDeleteSingleItem(item) },
				{ text: 'No' }
			]
		)
	}

	_renderItem(item, index, listSaldo) {
		let id = listSaldo.length - 1
		return (
			<TouchableOpacity style={Style.contentList} onPress={() => this._openMenu(item, listSaldo, index)}>
				<View style={{ flexDirection: 'column' }}>
					<View style={Style.textList}>
						<View style={{ width: GlobalStyle.dimensionWidth / 3, padding: 5 }}>
							{
								item.status == 'increment' ?
									<View style={{ flexDirection: 'row', alignItems: 'center', margin: 10, }}>
										<Icon name='ios-add' type='ionicon' color={GlobalStyle.colorGreen} />
										<Text style={Style.textPlaceholder}>{GlobalFunction.addCommasOnly(item.amount)}</Text>
									</View>
									:
									item.status == 'decrement' &&
									<View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
										<Icon name='ios-remove' type='ionicon' color={GlobalStyle.colorRed} />
										<Text style={Style.textPlaceholder}>{GlobalFunction.addCommasOnly(item.amount)}</Text>
									</View>
							}
						</View>

						<View>
							<Text>{GlobalFunction.addCommas(item.saldo)}</Text>
						</View>
					</View>

					<View style={{ backgroundColor: 'white', height: 40, flexDirection: 'row' }}>
						<View style={{ flex: 2, padding: 10, }}>
							<Text style={{ textAlign: 'left' }}>{`Keterangan: ${item.note == '' ? '-' : item.note}`}</Text>
						</View>

						<View style={{ flex: 1, padding: 10, alignItems: 'flex-end' }}>
							<Text style={Style.textDate}>{item.date == GlobalFunction.getDateNow() ? 'Hari ini' : item.date}</Text>
						</View>
					</View>
				</View>
				{
					this.state.itemMenu == index ?
					<View>
						<View style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: 3 }} />
						<View style={Style.deleteContainer}>
							<TouchableOpacity onPress={() => this._onDeleteSingleItem(item)}>
								<Icon name='delete' type='ionicons' color={GlobalStyle.colorRed} />
							</TouchableOpacity>
						</View>
					</View>
					:
					null
				}
				<View style={Style.line}></View>
			</TouchableOpacity>
		)
	}


	render() {
		let listSaldo;
		let index;
		let totalAmount = 0
		if (this.props.detail.list.length !== 0) {
			listSaldo = this.props.detail.list.filter(data => {
				return data.user_id == this.props.navigation.state.params.item.id
			})
		} else {
			listSaldo = []
		}

		if (listSaldo.length !== 0) {
			index = listSaldo.length - 1
			totalAmount = listSaldo[index].saldo
		}

		return (
			<View style={Style.container}>
				<Header
					title={`Detail Saldo ${this.props.navigation.state.params.item.name}`}
					back
					delete
					onDelete={() => this.deleteItem(listSaldo)}
					onPress={() => this.onBack()}
				/>
				<View style={{ flex: 5 }}>
					{
						listSaldo.length !== 0 ?
							<FlatList
								data={listSaldo}
								extraData={this.state}
								keyExtractor={(item, index) => `list-item-${index}`}
								renderItem={({ item, index }) => this._renderItem(item, index, listSaldo)}
								onLayout={event => {
									this.frameHeight = event.nativeEvent.layout.height
									const maxOffset = this.contentHeight - this.frameHeight
			
									this.setState({maxScroll: maxOffset, scrollHeight: this.frameHeight}) 
								}}
								onContentSizeChange={(contentWidth, contentHeight) => {
									this.contentHeight = contentHeight
									const maxOffset = this.contentHeight - this.frameHeight
			
									this.setState({maxScroll: maxOffset, scrollHeight: this.frameHeight}) 
								}}
								onScroll={event => { 
									this.yOffset = event.nativeEvent.contentOffset.y
									this.setState({scrollPosition: this.yOffset})
			
									this.forceUpdate();
									LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
								}}
								onScrollEndDrag={event => { 
									this.yOffset = event.nativeEvent.contentOffset.y
									 this.setState({scrollPosition: this.yOffset}) 
								}}
							/>
							:
							<View style={Style.noDataStyle}>
								<Text>No Saldo</Text>
							</View>
					}
				</View>
				<View style={{ flex: 0.5, borderTopColor: 'rgba(0,0,0,0.3)', borderTopWidth: 1 }}>
				{parseFloat(this.state.scrollPosition) < parseFloat(this.state.maxScroll-65) &&
					<View style={Style.footerContainerAmount}>
						<View style={[Style.triangle, { borderLeftColor: totalAmount < 0 ? 'yellow' : 'green', }]} />
						<View style={[Style.footerTotalAmount, { backgroundColor: totalAmount < 0 ? 'yellow' : 'green', }]}>
							<Text style={{ color: totalAmount < 0 ? 'black' : 'white', fontWeight: '800' }}>{GlobalFunction.addCommas(totalAmount)}</Text>
						</View>
					</View>
				}
					<View style={Style.buttonContainer}>
						<View style={Style.buttonMinus}>
							<TouchableOpacity onPress={() => this._onMinusButton(listSaldo)}>
								<Icon name='ios-remove' type='ionicon' color='white' />
							</TouchableOpacity>
						</View>

						<View style={Style.buttonTransfer}>
							<TouchableOpacity onPress={() => this._onTransferButton(listSaldo)}>
								<Icon name='ios-sync' type='ionicon' color='white' />
							</TouchableOpacity>
						</View>

						<View style={Style.buttonPlus}>
							<TouchableOpacity onPress={() => this._onPlusButton(listSaldo)}>
								<Icon name='ios-add' type='ionicon' color='white' />
							</TouchableOpacity>
							<TouchableOpacity />
						</View>
					</View>
				</View>

				<ModalAmount
					visible={this.state.modalPlus}
					total={this.state.total}
					type='increment'
					user_id={this.props.navigation.state.params.item.id}
					closeModal={() => {this.setState({ modalPlus: false }), this.setState({ itemMenu: null })}}
				/>
				<ModalAmount
					visible={this.state.modalMinus}
					total={this.state.total}
					user_id={this.props.navigation.state.params.item.id}
					type='decrement'
					closeModal={() => {this.setState({ modalMinus: false }), this.setState({ itemMenu: null })}}
				/>
				<ModalTransfer
					visible={this.state.modalTransfer}
					title={this.props.navigation.state.params.item.name}
					total={this.state.total}
					user_id={this.props.navigation.state.params.item.id}
					type='decrement'
					closeModal={() => {this.setState({ modalTransfer: false }), this.setState({ itemMenu: null })}}
				/>
			</View>
		)
	}
}

const mapStateToProps = state => {
	return {
		saldo: state.saldo,
		detail: state.detail
	}
}

const mapDispatchToProps = dispatch => {
	return {
		deleteItem: (id) => dispatch({ type: 'DELETE_ITEM', id }),
		deleteSaldoAmount: (params) => dispatch({ type: 'DELETE_TOTAL_SALDO', params }),
		_onDeleteSingleItem: (item) => dispatch({ type: 'DELETE_SINGLE_ITEM', item })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailSaldo);

const Style = {
	container: {
		flex: 1,
	},
	noDataStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		flex: 1
	},
	buttonMinus: {
		flex: 1,
		backgroundColor: GlobalStyle.greenOpacity,
		justifyContent: 'center',
		height: '100%'
	},
	buttonTransfer: {
		flex: 1,
		backgroundColor: 'blue',
		justifyContent: 'center',
		height: '100%'
	},
	buttonPlus: {
		flex: 1,
		backgroundColor: GlobalStyle.redOpacity,
		justifyContent: 'center',
		height: '100%'
	},
	contentList: {
		flex: 1
	},
	textList: {
		backgroundColor: 'white',
		padding: 10,
		flexDirection: 'row',
		alignItems: 'center',
	},
	line: {
		backgroundColor: 'rgba(0,0,0,0.2)',
		height: 1
	},
	textPlaceholder: {
		color: 'rgba(0,0,0,0.2)',
		marginLeft: 5
	},
	dateContainer: {
		backgroundColor: 'rgba(255, 255, 10, 0.5)',
		height: 40,
		justifyContent: 'center',
		padding: 10
	},
	textDate: {
		color: 'black'
	},
	deleteContainer: {
		backgroundColor: 'white',
		height: 60,
		justifyContent: 'center',
	},
	footerTotalAmount: {
		
		width: 170,
		height: 30,
		justifyContent: 'center',
		paddingRight: 20,
		alignItems: 'flex-end',
	},
	triangle: {
		backgroundColor: 'transparent',
		borderLeftWidth: 30,
		borderRightWidth: 30,
		borderBottomWidth: 30,
		borderRightColor: 'transparent',
		borderBottomColor: 'transparent',
		transform: [
      {rotate: '180deg'}
    ]
	},
	footerContainerAmount: {
		position: 'absolute',
		bottom: 60,
		right: 0,
		flexDirection: 'row',
	}
}