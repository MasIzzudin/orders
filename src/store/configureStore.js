import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistReducer, persistStore } from 'redux-persist';

import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import reducers from '../reducers';
import { middleware } from '../router/appNavigation';

let persistConfig;

// yen ameh nambah state ng reducers, iki di aktifke!!

// if (__DEV__) {
//     persistConfig = {
//         key: 'root',
//         storage,
//         whitelist: []
//     }
// } else {
//     persistConfig = {
//         key: 'root',
//         storage
//     }
// }

// yen ameh nambah state ng reducers, iki di comment!!

persistConfig = {
    key: 'root',
    storage,
}

const persistReducers = persistReducer(persistConfig, reducers)

const middle = []
middle.push(thunk)
middle.push(middleware)

export default () => {
    let store = createStore(persistReducers, {}, applyMiddleware(...middle))
    let persistor = persistStore(store)

    return { store, persistor }
}



