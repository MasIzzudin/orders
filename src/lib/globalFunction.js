export const GlobalFunction = {
	getDateNow() {
		let months = ["Jan","Feb","Maret","April","Mei","Juni","Juli","Agust","Sep","Okt","Nov","Des"];
		const d = new Date()
		
		const date = `${d.getDate()}-${months[d.getMonth()]}-${d.getFullYear()}`

		return date
	},

	addCommas(nStr) {
    //console.log('Curr ', APIVar.varCurrency)
    nStr = parseFloat(nStr).toFixed(2)
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    var vCurrency = '';
    if (vCurrency == null || vCurrency == '') {
      vCurrency = 'Rp '
    }
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return vCurrency + ' ' + x1 + x2;
	},
	
	addCommasOnly(nStr) {
    //console.log('Curr ', APIVar.varCurrency)
    nStr = parseFloat(nStr).toFixed(2)
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    var vCurrency = '';
    if (vCurrency == null || vCurrency == '') {
      vCurrency = 'Rp '
    }
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
  },
}