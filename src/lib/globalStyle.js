import { Dimensions } from 'react-native';

export const GlobalStyle = {
	colorGreen: '#4eba07',
	colorRed: '#c60101',
	greenOpacity: 'rgba(78, 186, 7, 0.8)',
	redOpacity: 'rgba(198, 1, 1, 0.8)',

	dimensionWidth: Dimensions.get('window').width,
	dimensionHeight: Dimensions.get('window').height
}